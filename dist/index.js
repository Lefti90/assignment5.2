"use strict";
// Assignment 5.2: Pipeline Linter
// Create a new empty repository in GitLab. Clone it to your local machine, 
// and initialize a NPM project into it. If using TypeScript, install and configure TypeScript. 
// Install and configure ESLint. Add some program code. A simple Hello World will do.
// Add scripts for starting the program, and linting the code. If using TypeScript, 
// also include a script to build the code. Make sure everything works locally.
// Add a .gitlab-ci.yml file. It should have three stages: install (where you install the dependencies),
//  lint (where the linter is run), and deploy (with a placeholder logging message,
//      we don't actually deploy anything today). If using TypeScript, it should have 
//      an additional build stage. Commit and push your changes. 
// Go to GitLab and check that all the jobs run and pass.
console.log("Hello world!");
